# SAMUEL 2020 - Linux Ubuntu

Guias rápidos para uso geral da distro UBUNTU.

<hr>
<br>

# Conteúdos
1. [Atalhos no Ubuntu](#shortcuts)
2. [Terminal Cheat Sheet](#bash)
3. [Comandos de atualização](#updates)
4. [Anti-vírus Clamav e Interface Clamtk](#clamav)
5. [Firewall Ufw e Interface Gufw](#ufw)
6. [Fail2Ban](#fail2ban)
7. [PSAD](#psad)
8. [Logwatch](#logwatch)
9. [Arquivos RAR](#rar)
10. [Instalando Python](#python)
11. [Instalando Node](#node)
12. [VSCode](#vscode)
13. [Dualboot Considerações](#dualboot)
14. [Personalização do UBUNTU](#custom)
15. [Referências](#links)

<br>

# Atalhos no Ubuntu
<a name="shortcuts"></a>

1. Super Key (windows/command): Open activities search
2. Ctrl+Alt+T: Ubuntu terminal shortcut
3. Super+L or Ctrl+Alt+L: Locks the screen
4. Super+D or Ctrl+Alt+D: Show desktop
5. Super+A: Shows the application menu
6. Super+Tab or Alt+Tab: Switch between running applications
7. Super+Arrow keys: Snap windows
8. Super+M: Toggle notification tray
9. Super+Space: Change input keyboard (for multilingual setup)
10. Alt+F2: Run console
11. Ctrl+Q: Close an application window
12. Ctrl+Alt+arrow: Move between workspaces

<br>

# Terminal Cheat Sheet
<a name="bash"></a>

## Pastas e Arquivos

### pwd

Mostra o caminho da pasta atual.
```bash
pwd
```

### cd

Muda para o diretório selecionado. 

. = diretório atual

.. = diretório anterior

~ = /home/[user]

/ = diretório barra /
```bash
cd <dir>
cd ..
cd ~
cd /
```

### ls

Lista conteúdo da pasta. Com -la são apresentados mais detalhes e arquivos ocultos.
```bash
ls
ls -la
```

Utilizando tree como alternativa ao ls (list)
```bash
sudo apt install tree
tree
```

### mkdir

Cria nova pasta.
```bash
mkdir <dir>
```

### rm

Deletando arquivos e diretórios. Recursivo -r. Force -f.
```bash
rm <file>
rm -r <dir>
rm -rf <dir>
```

### mv

Mover ou renomear arquivos.
```bash
mv <file1> <file2>
```

### cp

Copiar arquivos ou pastas.
```bash
cp <file> <dir>
cp -r <dir1> <dir2>
```

## Busca

### find

Procura arquivos com nome *file* dentro da pasta *dir*.
```bash
find <dir> -name "<file>"
```

### grep

Retorna ocorrências de *text* dentro de *file*.

Com -rl retorna arquivos contendo *text* dentro de *dir*.
```bash
grep "<text>" <file>
grep -rl "<text>" <dir>
```

Exemplo de uso grep
```bash
ps aux | grep htop
```

## Processos

Processos rodando atualmente.
```bash
ps ax
```

Informações sobre os processos.
```bash
top
```

Usando htop. Interface melhorada.
```bash
sudo apt install htop
htop
```

Terminando um processo.
```bash
kill <pid>
```

Processos em porta específica *port*.
```bash
lsof -i :<port>
```

## Shell

Terminal Multiplexer com TMUX
```bash
sudo apt install tmux
tmux
```

Os comandos do tmux iniciam com Ctrl+b (tmux do this command)
```
Ctrl+b c = create new session
Ctrl+b , = rename session
Ctrl+b p = switch to previous session
Ctrl+b n = switch to next session
Ctrl+b w = list sessions
Ctrl+b % = split vertical
Ctrl+b :split-window = split horizontal
Ctrl+b d = detach from session
```

Comandos no terminal
```bash
tmux new -s nameofthesession 
tmux list-sessions
tmux attach -t nameofthesession
tmux attach-session -t 0
```

Obs.: Sessions abertas continuarão abertas e executando serviços mesmo após fechadas com Ctrl+b d.

## Rede

### Secure Shell

Conectando via ssh no *host* como *user*.
```bash
ssh <user>@<host>
```

### Client URL

Downloading do arquivo *url/file* via HTTP(s) ou FTP.
```bash
curl -o <url/file>
```

<br>

# Comandos de atualização
<a name="updates"></a>

```bash
sudo apt update
sudo apt upgrade
sudo apt full-upgrade
sudo apt autoremove
sudo apt autoclean
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y
```

<br>

# Anti-vírus Clamav e Interface Clamtk
<a name="clamav"></a>

Instalando o Clamav
```bash
sudo apt install clamav clamav-daemon
```

Se optar por utilizar a GUI do ClamTK
```bash
sudo apt install clamav clamav-daemon clamtk
```

Para verificação de extensões .rar
```bash
sudo apt install libclamunrar7
```

Atualizando a database
```bash
sudo systemctl stop clamav-freshclam.service
sudo freshclam
```

Realizando scan em um diretório
```bash
clamscan -r [diretório]
clamscan -r /home/fulano/minha_pasta
```

Verificando mais informações
```bash
clamscan -help
```

Para mais informações, verificar em [Diolinux antivirus no Linux](https://diolinux.com.br/2019/08/antivirus-no-linux-conheca-o-clamav.html)

<br>

# Firewall Ufw e Interface Gufw
<a name="ufw"></a>

Talvez seja necessário instalar o repositório abaixo antes de instalar ufw e gufw
```bash
sudo add-apt-repository universe 
sudo apt update -y
```

O firewall UFW já vem instalado padrão no UBUNTU, resta instalar a interface. Mas se necessário:

```bash
sudo apt install ufw
```

Instalando a interface
```bash
sudo apt install gufw -y
```

Habilitando o Firewall
```bash
sudo ufw enable
```

Para rejeitar qualquer entrada
```bash
sudo ufw default reject incoming
```

Exemplos comuns de regras para SSH, VNC (conexão local), mude o IP e porta conforme seu uso
```bash
sudo ufw allow from 12.34.56.78 to any port 22 proto tcp
sudo ufw allow from 12.34.56.78 to any port 5900 proto tcp
```

Deletando regras
```bash
sudo ufw delete 1
sudo ufw delete 2
```

Checar status para verificar que está habilitado e regras adicionadas
```bash
sudo ufw status
```

Para mais informações, verificar em [help.ubuntu Gufw](https://help.ubuntu.com/community/Gufw) ou [itsfoss Set up firewall Gufw](https://itsfoss.com/set-up-firewall-gufw/)


<br>

# Instalando Fail2Ban
<a name="fail2ban"></a>

Instalando
```bash
sudo apt install fail2ban
```

Alterações
```bash
sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
```

Para mais informações verificar em [digitalocean fail2ban](https://www.digitalocean.com/community/tutorials/how-fail2ban-works-to-protect-services-on-a-linux-server).

<br>

# Arquivos RAR
<a name="rar"></a>

Para instalar unrar (para extrair) e rar (para comprimir)
```bash 
sudo apt-get install unrar
sudo apt-get install rar
```

Para extrair
```bash
unrar x FileName.rar
```

Para comprimir novo arquivo
```bash
rar a ArchiveName File_1 File_2 Dir_1 Dir_2
```

Para atualizar um arquivo
```bash
rar u ArchiveName Filename
```

Verificando mais informações
```bash
unrar
```

Para mais informações, verificar em [itsfoss use rar ubuntu](https://itsfoss.com/use-rar-ubuntu-linux/)

<br>

# Instalando PSAD
<a name="psad"></a>

Instalando (UBUNTU Default repositories)
```bash
sudo apt-get install psad
```

Configurações básicas em /etc/psad/psad.conf
```bash
sudo nano /etc/psad/psad.conf
EMAIL_ADDRESSES     address1@domain.com, address2@other.com;
HOSTNAME            your_domain.com;
IPT_SYSLOG_FILE        /var/log/syslog;
```

Atualização de assinaturas
```bash
sudo psad --sig-update
```

Restart para guardar atualizações
```bash
sudo service psad restart
```

Verificando status
```bash
sudo service psad status
```

Para mais informações, verificar em [digitalocean](https://www.digitalocean.com/community/tutorials/how-to-use-psad-to-detect-network-intrusion-attempts-on-an-ubuntu-vps)

<br>

# Instalando Logwatch
<a name="logwatch"></a>

Instalando (UBUNTU Default repositories)
```bash
sudo apt-get install logwatch
```

Configurações básicas em /usr/share/logwatch/default.conf/logwatch.conf
```bash
sudo nano /usr/share/logwatch/default.conf/logwatch.conf
MailTo = sysadmin@mydomain.com
MailFrom = sysadmin@mydomain.com
Range = yesterday (pode mudar para today)
Detail = Low (Low, Medium, High)
DailyReport = No (para não receber relatórios diários)
```

Executando manualmente e gerando relatório
```bash
logwatch  [--detail  level  ] [--logfile log-file-group ] [--service service-name ] [--print]
   [--mailto address ] [--archives] [--range range  ]  [--debug  level  ]  [--save  file-name  ]
   [--logdir  directory ] [--hostname hostname ] [--splithosts] [--multiemail] [--output output-type ] [--numeric] [--no-oldfiles-log] [--version] [--help|--usage]
```

Exemplos comuns para gerar relatório
```bash
logwatch --detail Low --mailto email@address --service http --range today
logwatch --detail High --range yesterday
```

Para mais informações, verificar em [digitalocean](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-logwatch-log-analyzer-and-reporter-on-a-vps)

<br>

# Instalando Python
<a name="python"></a>

Instalando python 3.8
```bash
sudo apt-get install python3.8
```

Instalando a versão mais atual de pip
```bash
/usr/bin/python3.8 -m pip install -U pip --user
```

Instalações para VSCode
```bash
/usr/bin/python3.8 -m pip install -U flake8 --user
/usr/bin/python3.8 -m pip install -U autopep8 --user
/usr/bin/python3.8 -m pip install -U pylint --user
```

Bibliotecas adicionais
```bash
/usr/bin/python3.8 -m pip install -U matplotlib --user
/usr/bin/python3.8 -m pip install -U numpy --user
/usr/bin/python3.8 -m pip install -U scipy --user
/usr/bin/python3.8 -m pip install -U pytest --user
```

Para funcionamento correto do matplotlib
```bash
/usr/bin/python3.8 -m pip install -U Pillow --user
sudo apt-get install python3-tk
```

Ainda no VSCode, talvez seja necessário adicionar em settings.json

Assim a versão 3.8 será a considerada na interpretação
```json
{
    "python.pythonPath": "/usr/bin/python3.8"
    "code-runner.executorMap": {
        "python": "$pythonPath -u $fullFileName"
    }
}
```

Verificando versões
```bash
python --version
python3 --version
python3.8 --version
pip --version
pip3 --version

```

<br>

# Instalando Node
<a name="node"></a>

Talvez seja necessário instalar o curl
```bash
sudo apt install curl
```

Instalando Node
```bash
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install -y nodejs
```

Verificando versões
```bash
node -v
npm -v
```

Para mais informações, verificar em [github node source](https://github.com/nodesource/distributions/blob/master/README.md)

<br>

# VSCode
<a name="vscode"></a>

## Instalação
Download .deb do [Visual Studio](https://code.visualstudio.com/Download) e execute.

## Extensões

Extensões recomendadas que utilizo:

1. Code Runner
2. ESLint
3. Python
4. Python Preview
5. Dracula Official
6. Material Icon Theme
7. Beautify
8. Live Server
9. GitLens - Git supercharged
10. Todo Tree

## Configurações settings.json

Settings (Ctrl+,)/Extensions/JSON/Edit settings.json

Configs que uso:
```json
{
    "workbench.colorTheme": "Dracula",
    "workbench.iconTheme": "material-icon-theme",
    "files.autoSave": "afterDelay",
    "editor.wordWrap": "on",
    "editor.formatOnSave": true,
    "code-runner.executorMap": {
        "python": "/usr/bin/python3.8 -u $fullFileName"
    },
    "[javascript]": {
        "editor.defaultFormatter": "HookyQR.beautify"
    },
    "workbench.startupEditor": "newUntitledFile",
    "git.autofetch": false,
    "liveServer.settings.donotShowInfoMsg": true,
    "[html]": {
        "editor.defaultFormatter": "vscode.html-language-features"
    },
    "todo-tree.tree.showScanModeButton": false,
    "todo-tree.general.tags": [
        "FIXME",
        "TODO"
    ],
    "todo-tree.highlights.customHighlight": {
        "TODO": {
            "background": "green",
            "foreground": "white",
            "type": "tag",
            "iconColour": "green"
        },
        "FIXME": {
            "background": "black",
            "foreground": "red",
            "type": "tag",
            "icon": "alert"
        }
    }
}
```

## Shortcuts

1. super + d -> permite selecionar palavras iguais e editar ao mesmo tempo
2. shift + alt + arrowKey -> seleção de multiplas linhas pra baixo ou pra cima
3. alt + arrowKey -> movimenta toda a linha pra cima ou pra baixo
4. ctrl + shift + p -> view command palette

<br>

# Dualboot Considerações
<a name="dualboot"></a>

Para bloquear acesso aos diretórios do Windows através do UBUNTU:

1. Acessar DISCOS
2. Selecionar volumes do windows
3. Opções adicionais de partição
4. Editar opções de montagem
5. Adicionar configurações conforme abaixo

```
nosuid,nodev,nofail,x-udisks-auth,noauto,umask=777
```

![opcoes-montagem](https://gitlab.com/samucamp/samuel-2020-linux-ubuntu/-/raw/master/images/opcoes-montagem.png "opcoes-montagem")

<br>

# Personalização do UBUNTU
<a name="custom"></a>

## Personalizando o tema, icones, cursor e wallpaper
Acompanhando de [How to make Ubuntu look like MacOS Mojave! (Apple meets Linux) Ubuntu Customization Guide](https://www.youtube.com/watch?v=AHzGYG2XvMI), segue links para download abaixo:

System Theme (Mojave Dark) - https://www.gnome-look.org/p/1241688/

Wallpapers - https://osxdaily.com/2018/08/03/25-new-macos-mojave-wallpapers/

Cursor Theme - https://www.gnome-look.org/p/1148692

Alternative Mac Cursor theme - https://www.gnome-look.org/p/1148748/

Cupertino icon set - https://www.gnome-look.org/p/1102582

Mojave & High Sierra Plank Dock themes - https://www.gnome-look.org/p/1248226/

"Dash to Dock" Gnome Extension **(optional, better install from ubuntu store)** - https://extensions.gnome.org/extension/307/dash-to-dock/

## Personalizando o terminal

Acompanhando de [Terminal com Oh My Zsh, Spaceship, Dracula e mais](https://blog.rocketseat.com.br/terminal-com-oh-my-zsh-spaceship-dracula-e-mais/) da Rocketseat.

<br>

# Referências
<a name="links"></a>

> https://itsfoss.com/set-up-firewall-gufw/

> https://help.ubuntu.com/community/Gufw

> https://diolinux.com.br/2019/08/antivirus-no-linux-conheca-o-clamav.html

> https://itsfoss.com/use-rar-ubuntu-linux/

> https://github.com/nodesource/distributions/blob/master/README.md

> https://www.digitalocean.com/community/tutorials/how-fail2ban-works-to-protect-services-on-a-linux-server

> https://www.digitalocean.com/community/tutorials/how-to-use-psad-to-detect-network-intrusion-attempts-on-an-ubuntu-vps

> https://www.digitalocean.com/community/tutorials/how-to-install-and-use-logwatch-log-analyzer-and-reporter-on-a-vps

> https://linuxize.com/post/getting-started-with-tmux/

> https://www.maketecheasier.com/what-is-grep-and-uses/

> https://blog.rocketseat.com.br/terminal-com-oh-my-zsh-spaceship-dracula-e-mais/

> https://www.youtube.com/watch?v=AHzGYG2XvMI
