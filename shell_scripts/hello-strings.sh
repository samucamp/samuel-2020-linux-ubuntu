#!/bin/bash
# para executar o arquivo utilizando shebang, precisa liberar
# a permissao de execucao com o comando chmod u+x hello-strings.sh

# tambem é possível executar com /bin/bash/hello-strings.sh

# HELLO WORLD
# uso de echo e /bin/echo
# declarando strings

echo "Hello world"
type echo
type -a echo
/bin/echo "Hello world test path to echo"

SKILL="shell scripting"
echo "Testando uma string: ${SKILL}"
echo "Careful. SKILL=... cannot have spaces"

# This is script is used for variables practice.

WORD="script"
echo "${WORD}ing is fun!"
echo "$WORDing is fun!"
echo "the above doesnt work correctly"
echo "$WORD"
echo "Possible for variable reassignment"
WORD="read"
echo "${WORD}ing is fun!"
# comments
