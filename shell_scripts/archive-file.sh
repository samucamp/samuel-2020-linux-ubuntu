#!/bin/bash

# archive one input file to determined location

# archive location
DATW=`date +%Y-%m-%d`
archive = /opt/archives/$DATE

# confirm 1 argument only
if [ $# -ne 1 ]
then
	echo "Usage: $0 FILE"
	exit 1
fi

# make the archive (if doesnt exist)
# -p doesnt overlap existing folder, create parents directory if needed
mkdir -p $archive

# copy the input file to archive
# try rsync instead of cp
# try -avr instead of -r
cp -r $1 $archive

exit 0
