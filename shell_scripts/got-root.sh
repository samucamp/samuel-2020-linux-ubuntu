#!/bin/bash

# Determine if the user executing this script is the root user or not

# Display the UID
echo "Your UID is ${UID}."
# root is always UID 0

# Display if the user is the root user or not
# attention to spaces in IF statement
# double bracket is the new sintaxe to be used, may find others with single
# single brackets have some issues, better use double
if [[ "${UID}" -eq 0 ]] 
then
	echo "your are root."
else
	echo "you are not root."
fi
