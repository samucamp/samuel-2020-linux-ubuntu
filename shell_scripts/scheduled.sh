# CRONTAB
# utilizando o crontab para marcar tarefas

# pode ser aberto com comando abaixo (deve escolher editor)
# crontab -e

# o crontab tem o seguinte formato
# [minutos] [horas] [dias do mês] [mês] [dias da semana] [usuário] [comando] 
# Min Hr Dom Dow User Command

# exemplos:
40 * * * * * echo "Toda hora, quando ponteiro maior estiver em 8"

# Schedule update and reboot
50 16 * * 1 sudo apt-get update && sudo apt-get -y upgrade
00 17 * * 1 sudo reboot now
#

