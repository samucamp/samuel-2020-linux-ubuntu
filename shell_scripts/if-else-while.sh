#!/bin/bash

# demo if-else and while use in shell script

a=5
b=200
c=$(($a*$b))
let c=a*b

if [ $a -gt $b ]
then
  # do something
  echo a greater than b
  let c=1
else
  echo a less than b
  let c=5
fi

# loop ten times
while [ $c -le 10 ]; do
  echo Number $c
  let c++
done
