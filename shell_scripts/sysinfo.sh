#!/bin/bash

# this script displays info about the system

# tell user script starting
echo "Starting the sysinfo script"

# display the hostname
hostname

# display current date and time
date

# display the kernel released followed by the architecture
uname -r
uname -m

# display the disk usage in a human readable format
df -h

# end the script and warn user
echo "Stopping the sysinfo script"
