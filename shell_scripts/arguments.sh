#!/bin/bash
# para executar o arquivo utilizando shebang, precisa liberar
# a permissao de execucao com o comando chmod u+x arguments.sh

# tambem é possível executar com /bin/bash/arguments.sh

# access some arguments
# zero means the first argument: name of script
# 0=name script, 1=nome, 2=idade
echo The command entered was $0
echo My name is $1
echo -e “I am $2 years old \n\t $# arguments were passed in”
# -e libera formatacao da string
# \n pula linha
# \t cria espacamento tab na proxima linha

